# Desarrollo de un software lingüístico para los sistemas de escritura de las lenguas indígenas de Costa Rica

El proyecto _Teclado Chibcha_ surgió a raíz de las dificultades
tecnológicas con las que se enfrentan maestros y estudiantes indígenas
para escribir en medios digitales en sus respectivas lenguas autóctonas.
Este hecho disminuye las posibilidades para el establecimiento de una
educación multicultural y multilingüe en la población indígena
costarricense.

El problema se agrava cuando ante la imposibilidad de escribir
adecuadamente en la computadora algunas de las grafías, especialmente
marcas diacríticas, se tiende a suprimirlas. Ello conduce a una
confusión que afecta sobre todo al sistema de educación intercultural
que se imparte en las escuelas y colegios de los territorios indígenas
de Costa Rica. Además, entre los jóvenes aumenta el sentimiento de
exclusión al no tener la posibilidad de usar su lengua en los medios
informáticos.

_Teclado Chibcha_ es una herramienta de fácil instalación y uso, que
permite la introducción de los caracteres especiales que emplean la
mayoría de los sistemas de escritura de las lenguas indígenas de Costa
Rica y brinda una solución a un problema técnico que tiene al mismo
tiempo claras implicaciones lingüísticas, culturales y sociales.

Los principales beneficiarios de este software son los indígenas
costarricenses y en general todas aquellas personas interesadas en el
estudio de sus lenguas.

## La Familia chibcha

### Localización

![familia chibcha](http://www.teclado.bribri.net/imagenes/lenguaschibchas.jpg)

1.  Paya (pech)
2.  Rama
3.  Malecu (guatuso)
4.  Huetar (extinta)
5.  Cabécar
6.  Bribri
7.  Naso
8.  Boruca (extinta) (brunca)
9.  Ngäbere (guaymí, movere)
10. Bocotá (buglere)
11. Dorasque (extinta)
12. Chánquena (extinta)
13. Cuna (kuna, tule)
14. Catío-nutabe (extinta) (antioqueño)
15. Chimila
16. Cogui (kogui, cábaga)
17. Damana (wiwa, sanká, guamaca)
18. Atanque (extinta)
19. Ica (bintucua, arhuaco, ika)
20. Barí (dobocubí, motilón)
21. Tunebo (uwa)
22. Duit (extinta)
23. Muisca (extinta) (mosca, chibcha)
24. Tairona (extinta)

### Descripción

La familia chibcha comprende un amplio grupo de idiomas hablados por los
pueblos chibchas, cuyo territorio tradicional comprende en la actualidad
los siguientes países: Honduras, Nicaragua, Costa Rica, Panamá, Colombia
y Venezuela.

### Situación

Aunque varias lenguas chibchas están extintas, por causa del predominio
del español como lengua dominante, algunas lenguas han subsistido y
continuan hablándose, aunque de manera minoritaria, y son hoy motivo de
estudio y enseñanza.

### Clasificación

Entre los lingüistas que han contribuido a demostrar el parentesco de
las denominadas lenguas chibchenses se encuentran Cassani, Uhle, y
especialmente el lingüista costarricense Adolfo Constenla Umaña, quien
ha propuesto una detallada clasificación.

### Lenguas Soportadas por el Teclado Chibcha

En Costa Rica se hablan actualmente seis lenguas de la estirpe
chibchense:

-   cabécar (8.500 hablantes)
-   guatuso o maleku (300 hablantes)
-   bribri (6.000 hablantes)
-   movere o guaymí (154.389, 152.217 en Panamá y 2.172 en Costa Rica
-   Teribe (3.000 hablantes)
-   Bocotá (3.373)

## Fuentes Unicode

### Fuente Charis Sil

[Charis
Sil](http://scripts.sil.org/cms/scripts/page.php?item_id=CharisSIL_Technical)
es una fuente
[Unicode](http://www.unicode.org/standard/WhatIsUnicode.html) que
combina de manera óptima la marcas suprasegmentales, además de
proporcionar muchos otros caracteres y símbolos útiles para los
lingüistas. Además, a diferencia de Doulos, es una fuente que ofrece una
tipografía mucho más profesional en los documentos impresos. Tiene
varios estilos: regular, cursiva, negrita y cursiva negrita.

Charis Sil es un tipo de letra con serifa muy similar a Bitstream
Charter, una de las primeras fuentes diseñadas específicamente para las
impresoras láser. Es una fuente no propietaria, desarrollada por [Summer
Institute of Linguistics, Inc](http://www.sil.org/), que usa un tipo de
licencia conocida como como _open font licence_, que permite su
distribución y modificación.

### Fuente Doulos

[Doulos
SIL](http://scripts.sil.org/cms/scripts/page.php?item_id=DoulosSILfont)
es una fuente
[Unicode](http://www.unicode.org/standard/WhatIsUnicode.html) que
combina de manera óptima la marcas suprasegmentales, además de
proporcionar muchos otros caracteres y símbolos útiles para los
lingüistas.

Doulos es un tipo de letra con serifa, muy similar a Times. Es una
fuente no propietaria, desarrollada por [Summer Institute of
Linguistics, Inc](http://www.sil.org/), que usa un tipo de licencia
conocida como como _open font licence_, que permite su distribución y
modificación.

## Uso del Teclado Chibcha

La aplicación que hemos desarrollado consta de dos partes: la fuente
Charis SIL y un mapa del teclado. En otras palabras, nuestra aplicación
instala en su sistema la fuente Charis SIL y proporciona un método de
entrada a través del teclado que tiene la disposición que se ve en el
siguiente diagrama.

Lo anterior no habría sido posible sin la utilización de una fuente
Unicode que a su vez permita apilar correctamente las marcas diacríticas
(marcas suprasegmentales) que se usan en la ortografía de muchas de
estas lenguas.

![mapa del teclado](http://www.teclado.bribri.net/imagenes/teclado-chibchacoloreado.jpg)

[Descargar diagrama del teclado en PDF](http://www.teclado.bribri.net/mapa-teclado-chibcha.pdf)

Para usar apropiadamente este teclado, por ejemplo, en editores de
texto, debe seleccionar como tipo de letra la fuente Charis Sil.

Para colocar diacríticos (tildes, guiones, etc.) digite primero la letra
y a continuación los diacríticos que quiere insertar.

Puede apilar varios diacríticos en una misma letra.

Las grafías (letras o diacríticos) ubicadas en la parte superior se
obtienen digitando la tecla la mayúscula (Shift) y la tecla que tiene la
grafía.

Las grafías (letras o diacríticos) ubicadas en la parte inferior derecha
se obtienen digitando la tecla ALTGR y la tecla de la grafía.

Las grafías (letras o diacríticos) ubicadas en la parte superior derecha
se obtienen mediante la combinancin AltGR + Shif + la tecla que tiene la
grafía.

## Grafías

### Marcas diacríticas combinables

| Código Unicode | Descripción                                        | Grafía | Teclas                 |
| ---------------| ---------------------------------------------------| -------| -----------------------|
| U+0300         | Acento grave combinable                            | [ò]  | VK\_OEM\_1             |
| U+0301         | Acento agudo combinable                            | [ó]   | VK\_OEM\_7             |
| U+0302         | Acento circunflejo combinable                      | [ô]    | VK\_OEM\_1 + Mayúscula |
| U+030C         | Acento anticircunflejo combinable                  | [ǒ]    | Shif + VK\_3           |
| U+0308         | Diéresis combinable                                | [ö]   | VK\_OEM\_7 + Mayúscula |
| U+0331         | Acento largo inferior combinable                   | [o̱]    | VK\_OEM\_MINUS         |
| U+0303         | Tílde combinable                                   | [õ]    | Shif + VK\_OEM\_MINUS  |
| U+0304         | Acento largo combinable                            | [ō]    | AltGr + VK\_OEM\_MINUS |
| U+0339         | Semicírculo inferior derecho combinable            | [o̹]    | VK\_OEM\_2             |
| U+031C         | Semicírculo inferior izquierdo combinable          | [o̜]    | Shif + VK\_OEM\_2      |
| U+0361         | Acento breve doble invertido combinable o ligadura | [o͡]    | AltGr + VK\_8          |
| U+02BC         | Apóstrofo modificador de letra                     | [oʼ]  | AltGr +VK\_OEM\_4      |
| U+02b0         | Letra modificadora minúscula h                     | [ʰo]  | AltGr + VK\_H          |

### Algunos caracteres IPA útiles para la trasncripción de las lenguas chibchas de Costa Rica

| Código Unicode | Descripción                                      | Grafía | Teclas                    |
| ---------------| -------------------------------------------------| -------| --------------------------|
| U+0294         | Letra latina que indica cierre glotal            | [ʔ]    | AltGr + Shif + VK\_OEM\_4 |
| U+027E         | Letra latina minúscula r con anzuelo             | [ɾ]    | AltGr + VK\_R             |
| U+027A         | Letra latina minúscula girada r con pierna larga | [ɺ]   | AltGr + Shif + VK\_R      |
| U+0283         | Letra latina minúscula esh                       | [ʃ]    | AltGr + VK\_S             |
| U+0272         | Letra minúscula n con gancho a la izquierda      | [ɲ]    | AltGr + VK\_OEM\_3        |
| U+0245         | Letra latina mayúscula girada V                  | [Ʌ]    | AltGr + VK\_L             |
| U+02A6         | Dígrafo ts                                       | [ʦ]    | AltGr + VK\_T             |
| U+02A4         | Dígrafo dezh                                     | [ʤ]    | AltGr + VK\_Y             |
| U+02A7         | Dígrafo tesh                                     | [ʧ]    | AltGr + VK\_C             |
| U+025B         | Letra latina minúscula e abierta                 | [ɛ]    | AltGr + VK\_E             |
| U+028A         | Letra latina minúscula ípsilón                   | [ʊ]    | AltGr + VK\_U             |
| U+0289         | Letra latina minúscula u con barra               | [ʉ]    | AltGr + Shif + VK\_U      |
| U+0254         | Letra latina minúscula o abierta                 | [ɔ]    | AltGr + VK\_O             |
| U+028C         | Letra latina minúscula girada v                  | [ʌ]    | AltGr + Shif + VK\_O      |

## Requisitos

Sistemas operativos requeridos para la instalación del teclado chibcha
1.0

-   Windows 7
-   Windows Vista
-   Windows XP
-   Windows Server 2003
-   Windows 2000 Professional
-   Windows NT 4.0 workstation

El teclado chibcha funciona correctamente en las siguientes
aplicaciones:

-   Microsoft Office 2003 y Office 2010
-   OpenOffice
-   Bloc de notas
-   Word pad
-   Paint
-   Dreamweaver

## Descargar

Windows
-   [Teclado Chibcha V 2.0](http://www.teclado.bribri.net/TecladoChibcha-2.exe) (2011)
-   [Teclado Chibcha V 1.0](http://www.teclado.bribri.net/TecladoChibcha.exe) (2009)

Fuente para Microsoft Keyboard Layout Creator 1.4
-   [Español (distribución española)](codigo/spa_ESP_cba.klc)
-   [Español (distribución latinoamericana)](codigo/spa_CRI_cba.klc)
-   [Inglés (distribución internacional)](codigo/eng_USA_cba.klc)

Linux
-   [Teclado Chibcha V 1.0](codigo/spa_ESP_cba.xkb) (2012)

Android
-   [Multiling O Keyboard +
    emoji](https://play.google.com/store/apps/details?id=kl.ime.oh) de
    Google Play
-   En Android se debe instalar la app [Multiling O Keyboard +
    emoji](https://play.google.com/store/apps/details?id=kl.ime.oh) y
    una vez instalada utilizar este [DIY Layout](codigo/Multiling_O_keyboard_DIY.json)
        
## Configuración

El teclado chibcha está disponible para Windows (2000, XP, Vista,
Windows7, Windows8, Windows10), y también para Linux. En el futuro
esperamos contar con una versión para MacOS X.

La configuración del Teclado Chibcha a partir de Windows7 es automática,
solamente se debe seleccionar el Español de Costa Rica y el Teclado
Chibcha en la barra del idioma, en Windows XP es necesario configurar
manualmente la barra del idioma siguiendo los pasos que se detallan a
continuación.

### Linux (Debian y Ubuntu)

#### Copiar el archivo cr\_chibcha en el directorio /usr/share/X11/xkb/symbols/ Es necesario ser root.

sudo cr\_chibcha chib /usr/share/X11/xkb/symbols

Para otras distribuciones averiguar donde se alojan los archivos de
configuración del paquete XKB (keyboard mapping package)

#### Instalar una fuente Unicode adecuada para proporcionar el adecuado apilamiento de varios diacríticos en un mismo caracter (Ej: Doulos, Charis-Sil, Lucida Sans Unicode)

\$ wget http://packages.sil.org/sil.gpg\
\$ sudo apt-key add sil.gpg\
\$ sudo apt-add-repository \"deb http://packages.sil.org/ubuntu/
\$(lsb\_release -sc) main\"\
\$ sudo apt-get update\
\$ sudo apt-get install fonts-sil-charis

#### Cambiar la distribución del teclado con el comando setxkbmap

Para cambiar al teclado chibcha ejecute los siguientes comandos en la
terminal:

\$ setxkbmap cr\_chibcha

Para cambiar al teclado en español

\$ setxkbmap es

### Windows (XP)

#### Asegúrese de que el teclado ha sido instalado y registrado por el sistema:

Panel de control \| Configuración regional y de idiomas \| idiomas \|
detalles

![ver el teclado chibcha](imagenes/teclados.jpg)

Aquí puede agregar o quitar varios idiomas y teclados.

#### Instale los archivos de idioma de escritura compleja de Windows. Para ello debe ir a

Panel de control \| Configuración regional y de idiomas \| idiomas

Active la opción \"instalar archivos de idiomas de escritura compleja de
derecha a izquierda incluyendo el Tailandés\". El sistema le pedirá el
CD de instalación de Windows. Inserte el CD y espere que el sistema
copie los archivos que necesita.

![instalar archivos
adicionales](http://www.teclado.bribri.net/imagenes/instalar-archivos.jpg)

#### Configure la barra de idioma

En la ventana del paso 1, haga doble click en la barra de idioma que se
encuentra debajo.

Active la primera y la última opción: \"Mostrar la Barra de idioma en el
escritorio\" y \"Mostrar las etiquetas de la Barra de idioma\". Haga
doble click en aceptar.

![configurar barra de idioma](http://www.teclado.bribri.net/imagenes/barra.jpg)

#### Configure como desea alternar entre sus idiomas y teclados

Haga doble click en \"configuración de teclas\", que se encuentra la
lado de la \"barra de idioma\" (Ver cuadro de diálogo en el paso 2).
Aparecerá una ventana como esta:

![configuración avanzada de teclas](http://www.teclado.bribri.net/imagenes/teclas1.jpg){.ejemplo}

Haga doble click en \"cambiar secuencia de teclas\" y aparecerá otra
ventana, donde podrá habilitar la combinación de teclas que desee para
cada uno de sus idiomas y teclados instalados.

![cambiar secuencia de teclas](http://www.teclado.bribri.net/imagenes/teclas2.jpg)

#### Establezca las opciones avanzadas de texto: haga doble click en \"opciones avanzadas\", como se muestra en la imagen, para ello debe ir a

Panel de control \| Configuración regional y de idiomas \| idiomas \|
detalles \| opciones avanzadas

![opciones avanzadas de texto](http://www.teclado.bribri.net/imagenes/avanzadas.jpg)

Active la opción "Configuración de compatibilidad. Extender
compatibilidad con servicios avanzados de texto para todos los
programas".

Reinice el sistema.

## Recursos

### Mac OS X

- [Ukelele Mac OS X Keyboard Layout
Editor](http://scripts.sil.org/ukelele)

- [Instalación de un nuevo teclado en Mac OS X
10.2](http://developer.apple.com/technotes/tn2002/tn2056.html)

### Linux

- [Multilingual text on linux](http://www.jw-stumpel.nl/stestu.html#T4.8)

- [Creating custom keyboard layouts for X11 using XKB](http://hektor.umcs.lublin.pl/~mikosmul/computing/articles/custom-keyboard-layouts-xkb.html)

### Windows

- [Microsoft Keyboard Layout Creator 1.4](https://www.microsoft.com/en-us/download/details.aspx?id=22339)

- [Keyboard Layout Manager](http://www.klm32.com/index.html)

## Enlaces de interés

- Artículo [Teclado Chibcha: un software lingüístico para los sistemas de escritura de las lenguas bribri y cabécar](https://revistas.ucr.ac.cr/index.php/filyling/article/view/1110/1171)

## Contacto

Si necesita ayuda para la instalación o utilización del teclado chibcha,
por favor no dude en comunicarse con la autora de la aplicación.

- Sofia Flores Solórzano (sofia.flores.s@gmail.com)
  http://www.teclado.bribri.net/
